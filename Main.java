import java.util.Arrays;
import java.util.Random;

// Joseph Hentges
// Main.java

/**
 * Test Threads in Java.
 * 
 * @author Joseph Hentges
 * Augsburg University
 * CSC 450 - Programming Languages
 * Fall Semester 2019
 * December 12, 2019
 */

public class Main
{
  public static void main(String[] args)
  {
    // get an array with random values
    int[] randomArray = getRandomArray(50000000);
    CurrentLowest lowest = new CurrentLowest();

    // an array to hold the threads used
    int count = 5;
    RandomArray[] threads = new RandomArray[count];

    // create the threads
    for(int i = 1; i <= count; i += 1) {
      threads[i-1] = new RandomArray(randomArray, i, count, lowest);
    }

    // start all of the threads
    try {
      for(int i = 0; i < count; i += 1) {
        threads[i].start();
      }
      System.out.println("Finished Starting Threads");
    } catch (Exception e) {
      e.printStackTrace();
    }

    // wait for all threads to finish
    for (Thread thread : threads) {
      try {
        thread.join();
      } catch (Exception e) {
        e.printStackTrace();
      }
    }

    // print my found lowest in the array
    System.out.println("Lowest found is: " + lowest.getLowest());

    // sort the array and print the lowest
    Arrays.sort(randomArray);
    System.out.println("Lowest found after sorting is: " + randomArray[0]);

    System.out.println("Threads Found == Sort Found : " + (lowest.getLowest() == randomArray[0]));
  }

  /***
   * Generate an array with random values.
   * @param length - the length of the array to be generated
   * @return - the array
   */
  public static int[] getRandomArray(int length)
  {
    int[] array = new int[length];
    Random random = new Random();

    for (int i = 0; i < array.length; i += 1) {
      array[i] = random.nextInt();
    }

    return array;
  }
}