class CurrentLowest extends Thread
{
  private int lowest;

  public CurrentLowest() {
    lowest = Integer.MAX_VALUE;
  }

  public void setLowest(int lowest) {
    if (lowest < this.lowest) {
      this.lowest = lowest;
    }
  }

  public int getLowest() {
    return lowest;
  }
}