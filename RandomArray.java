public class RandomArray extends Thread
{
  int[] array;
  int threadNumber;
  int numberThreads;
  CurrentLowest currentLowest;

  public RandomArray(int[] array, int threadNumber, int numberThreads, CurrentLowest currentLowest) {
    this.array = array;
    this.threadNumber = threadNumber;
    this.numberThreads = numberThreads;
    this.currentLowest = currentLowest;
  }

  @Override
  public void run() {
    int spacer = array.length / numberThreads;
    int startIndex = spacer * (threadNumber - 1);
    int endIndex = spacer * threadNumber;

    for (int i = startIndex; i < endIndex; i += 1) {
      synchronized(currentLowest) 
      { 
        currentLowest.setLowest(array[i]);
      } 
    }
  }
}