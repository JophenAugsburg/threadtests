
/**
 * 
 * @author Joseph Hentges
 */
public class YakYak extends Thread {
  private String speach;

  public YakYak(String speach) {
    this.speach = speach;
  }

  @Override
  public void run() {
    int times = 1;
    while (times < 6) {
      System.out.println(speach + " " + times);
      times += 1;
    }
  }
}