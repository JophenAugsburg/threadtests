// Joseph Hentges
// Main.java

/**
 * Test Threads in Java.
 * 
 * @author Joseph Hentges
 * Augsburg University
 * CSC 450 - Programming Languages
 * Fall Semester 2019
 * December 12, 2019
 */

public class Main
{
  public static void main(String[] args)
  {
    YakYak cat = new YakYak("MEOW");
    YakYak dog = new YakYak("RUFF");
    YakYak cow = new YakYak("MOOO");

    try {
      cat.start();
      dog.start();
      cow.start();
      System.out.println("Finished");
    } catch (Exception e) {
      e.printStackTrace();
    }

    System.out.println("Really at the end...");
  }
}